
# API Fuzzer

HTTP API discovery and testing tool

## Development Notes

### Features

```
  Status      | Feature
 -------------+---------------------------------------------------------
  COMPLETE    | URI enumeration and mapping
  PLANNING    | Probe to find allowed HTTP method(s) for an endpoint
  PLANNING    | Probe to discover input parameters (+cookie, header, etc..)
  PLANNING    | Send fuzzy data to an endpoint via the discovered input params
```

### Libraries

- https://wfuzz.readthedocs.io/en/latest/user/advanced.html#filter-language

### Process

- optionally authorise as a user

- discovery phase
  - map available routes using HEAD|OPTIONS
    - FUZZ URI segments
    - limit recursion depth
    - store list of discovered routes

- probe phase (idea)
  - for each found route
    - attempt create using POST
    - attempt retrieve using GET
    - attempt update using PUT
    - attempt delete using DELETE
    - attempt to gracefully handle 400 errors
      - parsing responses for parameter discovery
      - try parameter injection for 422 responses

#### Segment Generator

A URI consists of segments. For example: `/a/b/c`

- `a` is segment 0
- `b` is segment 1
- `c` is segment 2

URI segment format string: `/<prefix><word><suffix><extension>/...`

`<word>` is generated based on rules for combined verbs and nouns
`<prefix>` is an arbitrary string to prepend to generated words
`<suffix>` is an arbitrary string to append to generated words
`<extension>` is an arbitrary file ext. to append to the *last* segment

### Result Object

All possible values for a HTTP request transaction

- url
  - url.scheme        # http
  - url.netloc        # www.google.com
  - url.path          # /dir/test.php
  - url.params
  - url.query         # id=1
  - url.fragment
  - url.ffname        # test.php
  - url.fext          # .php
  - url.fname         # test
  - url.hasquery      # Returns true when the URL contains a query string.
- method
- scheme
- host
- content
- raw_content
- cookies.request
- cookies.response
- headers.request
- headers.response
- params.get
- params.post
- pstrip
- is_path
- reqtime

Derived...

- request_content_type
- response_content_type

### Ideas

- ~~prefix and suffix~~
- ~~file extension~~
- ~~allow mulitple seperators and loop throught them (eg. --separator '_' --separator '-')~~
- ~~remove py-flags library~~
- ~~noun pluraliser~~
- ~~move self.options{} to self.options => Options() and use self.options.name~~
- when enter no URL, defaults to localhost and if in container, timeouts
- dictionary related improvements and fixes
  - parameter generator (is_<noun>, is_<verb>ed, is_<adj>, ...)
  - create: adjectives.tiny,adjectives.common,adjectives.financial,adjectives.travel,...
  - add wordlists of almuns and punctuaction for brute force character by character discovery?
  - ~~use `--sector financial` to control which wordlists are used~~
  - ~~generate past-participal for a verb~~
  - ~~create: verbs.tiny,verbs.common,verbs.financial,verbs.travel,...~~
  - ~~create: nouns.tiny,nouns.common,nouns.financial,nouns.travel,...~~
- uri scanning process related
  - use same mode for following segments as the first segment
    - don't do /product/UPDATE-PRODUCT .....
    - store a mode field in each record. then select using those modes
- validate input (eg. does -m exist, is -M a valid HTTP verb)
- how to package a python app? pip install
- how to output a file when running on a container? volumes are owned by uid=0
