
#@echo $(git rev-parse HEAD) > rootfs/opt/paf/version

build:
	@docker build \
		-t paf:latest \
		$$(pwd)

shell:
	@docker run --rm -it \
		--name paf \
		--hostname paf \
		-v $$(pwd)/rootfs/opt/paf:/opt/paf \
		--entrypoint /bin/bash \
		paf:latest

dbshell:
	@docker exec -it \
		paf \
		/usr/bin/sqlite3 /tmp/paf.db
