
from enums import Sector, Mode

class Options:

    # options is a docopt.docopt obj
    def __init__(self, options):

        self.baseurl = options.get('URL')
        self.depth = int(options.get('--depth'))
        self.dryrun = bool(options.get('--dryrun'))
        self.extension = options.get('--extension')
        self.help = bool(options.get('--help'))
        self.method = options.get('--method')
        self.mode = options.get('--mode')
        self.operation = options.get('--operation')
        self.past = bool(options.get('--past'))
        self.plural = bool(options.get('--plural'))
        self.prefix = options.get('--prefix')
        self.present = bool(options.get('--present'))
        self.sector = options.get('--sector')
        self.separator = options.get('--separator')
        self.suffix = options.get('--suffix')
        self.suppress = bool(options.get('--suppress'))
        self.verbable = bool(options.get('--verbable'))
        self.version = bool(options.get('--version'))

        # TODO validate user input options

        if not self.baseurl:
            self.baseurl = 'http://localhost'

        # make sure these have a least one item
        # because it's easy to write iterations
        if not self.extension:
            self.extension.insert(0, '')

        if not self.prefix:
            self.prefix.insert(0, '')

        if not self.suffix:
            self.suffix.insert(0, '')

        if not self.separator:
            self.separator.insert(0, '-')

        # handle option `all` modes
        if len(self.mode) == 1 and 'all' in self.mode:
            self.mode = list(filter(
                lambda attr: not callable(attr) and attr[0] != '_',
                dir(Mode)
            ))

        # handle option `all` sectors
        if len(self.sector) == 1 and 'all' in self.sector:
            self.sector = list(filter(
                lambda attr: not callable(attr) and attr[0] != '_',
                dir(Sector)
            ))

