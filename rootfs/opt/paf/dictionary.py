
import inflect

class Dictionary:

    def __init__(self, sectors = ['common']):

        # wordpools
        self.nouns = []
        self.nouns_plural = []
        self.verbs = []
        self.adj_past_participle = []
        self.verbs_present = []
        self.adj = []

        # format string for wordlists paths
        dictpath = '/opt/paf/dictionary/%s.%s.txt'

        # use inflect to pluralise and create
        # the present_participal of a verb
        # https://pypi.org/project/inflect/
        self.inflector = inflect.engine()

        # load nouns for each sector
        for sector in sectors:
            self.nouns = list(set(self.nouns + [
                word.strip() for word in open(
                    dictpath % (sector, 'nouns')
                )
            ]))

        # generated pluralised nouns
        self.nouns_plural = [
            self.inflector.plural_noun(
                word.strip()
            ) for word in self.nouns
        ]

        # load verbs for each sector
        for sector in sectors:
            self.verbs = [
                word.strip() for word in open(
                    dictpath % (sector, 'verbs')
                )
            ]

        # load adjectives for each sector
        for sector in sectors:
            self.adj = list(set(self.adj + [
                word.strip() for word in open(
                    dictpath % (sector, 'adjectives')
                )
            ]))

        # generate present_participle adjectives (eg. adding)
        self.adj_present_participle = [
            self.inflector.present_participle(
                word.strip()
            ) for word in self.verbs
        ]

        # generate past_participle adjectives (eg. added)
        self.adj_past_participle = [
            self.past_participle(
                word.strip()
            ) for word in self.verbs
        ]

        # generate verb -able adjectives (eg. addable)
        self.adj_verbable = [
            self.verb_to_adjective(
                word.strip()
            ) for word in self.verbs
        ]

    def getNouns(self, original=True, plural=False):
        data = []
        if original:
            data = data + self.nouns
        if plural:
            data = data + self.nouns_plural
        if not data:
            raise Exception('All nouns are filtered out. Check your command line options.')
        return data

    def getVerbs(self, original=True, past=False, present=False, verbable=False):
        data = []
        if original:
            data = data + self.verbs
        if past:
            data = data + self.adj_past_participle
        if present:
            data = data + self.adj_present_participle
        if verbable:
            data = data + self.adj_verbable
        if not data:
            raise Exception('All verbs are filtered out. Check your command line options.')
        return data

    def getAdjectives(self, original=True):
        data = []
        if original:
            data = data + self.adj
        data = data + self.adj_present_participle
        data = data + self.adj_past_participle
        data = data + self.adj_verbable
        if not data:
            raise Exception('All adjectives are filtered out. Check your command line options.')
        return data

    # TODO find a library to create the past-participal instead of this hack
    def verb_to_adjective(self, v):
        return '%sable' % v

    # TODO find a library to create the past-participal instead of this hack
    # https://codegolf.stackexchange.com/questions/3915/write-a-function-that-returns-past-tense-of-given-verb#24035
    def past_participl_OLD(self, v):
        T,x,m='aeiou',"ed",v[-1];return[[[v+x,v+m+x][v[-2]in T and m and v[-3]not in T],[v+x,v[:-1]+"ied"][v[-2]not in T]][m=='y'],v+"d"][m=='e']

    # TODO find a library to create the past-participal instead of this hack
    # https://github.com/kaitlynnhetzel/past_tense_generator/blob/master/past_tense_generator.py
    def past_participle(self, to_past):
        irregular_forms =  {"go": "went","buy": "bought", "break": "broke", "sit": "sat", "come":"came", "eat": "ate", "sleep": "slept", "see": "saw", "pay": "paid", "sing": "sang", "tell": "told", "get": "got", "teach": "taught", "feel": "felt",
        "hear": "heard", "understand": "understood", "is": "was", "make": "made", "lose": "lost"}

        #these verbs are the same in both the present and pass tense
        single_forms =  ["cut", "put", "let", "hurt", "quit", "read", "broadcast", "hit", "cost", "spread"]

        #a vowel list, for reference
        self.vowels = ['a','e','i','o','u','y']

        if to_past.lower() in irregular_forms:
            return irregular_forms[to_past.lower()]
        #checking single-form verbs
        elif to_past.lower() in single_forms:
            return to_past.lower()
        #checking for a verb ends in 'e' - slightly special case
        elif to_past[len(to_past) -1] == 'e':
            to_past = to_past + "d"
            return to_past
        #checking special case if verb ends in a y
        elif to_past[len(to_past) - 1] == 'y':

            if to_past[len(to_past) - 2] in self.vowels:
                to_past = to_past + "ed"
                return to_past
            else:
                temp = list(to_past)
                temp[len(to_past)-1] = 'i'
                to_past = "".join(temp)
                to_past = to_past + "ed"
                return to_past
        #checking if verb needs a repeated consonant before "ed"
        elif self.needsDoubleConsonant(to_past):
            to_past = to_past + to_past[len(to_past) -1] + "ed"
            return to_past
        #typical case - just add "ed"
        else:
            to_past = to_past + "ed"
            return to_past

    # this is a dependancy for `past_participle`
    # TODO find a library to create the past-participal instead of this hack
    def needsDoubleConsonant(self, verb):
        count = 0

        #suffixes of root words that will never be repeated
        end_hushers = ["x", "lk", "sh", "ch", "ck", "h", "k", "nt", "lp", "wn", "st"]

        #since multiple vowels are usually not mono-syllabic, we are counting them to see if  a word is monosyllabic
        #along that same note, it is not common for a mono-syllabic word with two adjacent vowels (i.e - beam) to then
        #be followed by a double consonant. if any edge cases arise, they will be added in later
        for letter in verb:
            if letter in self.vowels:
                count = count + 1

        if count > 1:
            return False
        else:
            last_two = verb[len(verb)-2] + verb[len(verb)-1]
            last = verb[len(verb)-1]
            #if the last letter is one that will never be repeated
            if last in end_hushers:
                return False
            #if the suffix of the word will never need a repeated letter
            elif last_two in end_hushers:
                return False
            #if the last two letters of the verb are already a repeated letter
            elif last == verb[len(verb)-2] :
                return False
            return True
