
import sqlite3

class SimpleDb:

    def __init__(self):
        # self.connection = sqlite3.connect(':memory:')
        self.connection = sqlite3.connect('/tmp/paf.db')
        self.create()

    def create(self):
        self.query('DROP TABLE IF EXISTS results')
        self.query('''
            CREATE TABLE results (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                reqtime REAL,
                depth INTEGER,
                method STRING,
                reqhdrs STRING,
                pstrip STRING,
                url STRING,
                fext STRING,
                ffname STRING,
                fname STRING,
                fragment STRING,
                hasquery BOOL,
                hostname STRING,
                netloc STRING,
                params STRING,
                password STRING,
                path STRING,
                port STRING,
                query STRING,
                scheme STRING,
                username STRING,
                status INTEGER,
                resphdrs STRING,
                contype STRING,
                contlen INTEGER,
                content TEXT
            );
        ''')
        self.commit()

    def query(self, sql, params = ()):
        statement = self.connection.cursor()
        return statement.execute(sql, params)

    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()

