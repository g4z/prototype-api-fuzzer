#!/usr/bin/env python
"""
              __
 _ __   __ _ / _|
| '_ \\ / _` | |_
| |_) | (_| |  _|
| .__/ \\__,_|_|
|_| api fuzzer

Usage: paf [-hknptTV] [--depth DEPTH] [--extension EXT]... [--method METHOD]
           [--mode MODE]... [--operation OPERATION] [--prefix PREFIX]...
           [--sector SECTOR]... [--separator SEP]...
           [--suffix SUFFIX]... [URL]

Arguments:
  URL   base url of the api

Options:

  -d --depth DEPTH     max segment depth [default: 2]
  -E --extension EXT   append file ext. to last segment
  -h --help            show this help message and exit
  -M --method METHOD   use this http request method [default: HEAD]
  -m --mode MODE       request URI enumeration mode [default: noun]
                       use `-m` multiple times for multiple modes
                       see `Modes` section below for more information
  -p --plural          pluralise generated segment nouns [default: False]
  -P --prefix PREFIX   prepend string to each segment
  -n --dryrun          print the generated URIs only but do no requests
  -s --separator SEP   character used for word seperation
  -S --suffix SUFFIX   append string to each segment
  -k --suppress        do not use the original dictionary word
  -t --present         generate present adjectives from verbs (eg. updating)
  -T --past            generate past adjectives from verbs (eg. updated)
  -V --verbable        generate able-type adjectives (eg. updateable)
  -x --sector SECTOR   use sector target wordlist [default: common]
                       use `-x` multiple times for multiple sectors
                       see `Sectors` section below for more information
  -z --operation OP    select the operating mode [default: path]
                       see `Operations` section below for more info

Operations:

  The following operational modes are available...

    Operation      | Description
   ----------------+--------------------------------------------------------
    path           | Scans an API to discover existing URIs/paths
    method         | Scans an endpoint for accepted HTTP methods
    param          | Scans an endpoint to determine valid input parameters
    input          | Injects fuzzed data into discovered input parameters

Modes:

  URI path segments are generated using `modes`. The table shows
  each mode and an example of a segment generated using that mode

    Name           | Example
   ----------------+------------------------
    noun           | product
    verb           | update
    Noun           | Product
    Verb           | Update
    NOUN           | PRODUCT
    VERB           | UPDATE
    nounnoun       | productcategory
    nounverb       | productupdate
    verbnoun       | updateproduct
    nounXnoun      | product-category
    nounXverb      | product-update
    verbXnoun      | update-product
    nounNoun       | productCategory
    nounVerb       | productUpdate
    verbNoun       | updateProduct
    nounXNoun      | product-Category
    nounXVerb      | product-Update
    verbXNoun      | update-Product
    nounNounXVerb  | productCategory-Update
    verbXNounNoun  | update-ProductCategory
    NounNoun       | ProductCategory
    NounVerb       | ProductUpdate
    VerbNoun       | UpdateProduct
    NounXNoun      | Product-Category
    NounXVerb      | Product-Update
    VerbXNoun      | Update-Product
    NounNounXVerb  | ProductCategory-Update
    VerbXNounNoun  | Update-ProductCategory
    NOUNNOUN       | PRODUCTCATEGORY
    NOUNVERB       | PRODUCTUPDATE
    VERBNOUN       | UPDATEPRODUCT
    NOUNNOUNVERB   | PRODUCTCATEGORYUPDATE
    VERBNOUNNOUN   | UPDATEPRODUCTCATEGORY
    NOUNXNOUN      | PRODUCT-CATEGORY
    NOUNXVERB      | PRODUCT-UPDATE
    VERBXNOUN      | UPDATE-PRODUCT
    NOUNNOUNXVERB  | PRODUCTCATEGORY-UPDATE
    VERBXNOUNNOUN  | UPDATE-PRODUCTCATEGORY
    nounNounVerb   | productCategoryUpdate
    verbNounNoun   | updateProductCategory
    NounNounVerb   | ProductCategoryUpdate
    VerbNounNoun   | UpdateProductCategory
    nounXnounXverb | product-category-update
    verbXnounXnoun | update-product-category
    NounXNounXVerb | Product-Category-Update
    VerbXNounXNoun | Update-Product-Category
    NOUNXNOUNXVERB | PRODUCT-CATEGORY-UPDATE
    VERBXNOUNXNOUN | UPDATE-PRODUCT-CATEGORY

  To specify multiple modes, use the `--mode` option multiple times

Sectors:

  A sector defines the dictionary/wordlist use to generate the
  URI segements. One or more sectors can be specificed. The default
  sector is `general`. The following values are valid sectors...

    Sector         | Description
   ----------------+------------------------
    all            | all
    auth           | login and authentication
    business       | general business words
    commerce       | words related to commerce
    common         | common set of words (small)
    education      | education and related
    financial      | financial, banking and related
    geek           | geek, programmer and dev words
    general        | general set of words (large)
    leisure        | leisure, social, travel, etc..
    tech           | technology related words
    tiny           | set of words for testing (tiny)

Examples:

    $ paf --dryrun --mode verbNoun http://localhost/api

    $ paf --mode verb --depth 1 http://localhost/api/product

    $ paf --mode verbXnoun --separator _ http://localhost/api

    $ paf --sector financial http://localhost/api

    $ paf --extension .php --extension .py http://localhost/api

    $ paf --prefix _ --suffix _backup http://localhost/api

    $ paf --method GET http://localhost/api

License:

  Copyright 2019 Gareth Williams

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated
  documentation files (the "Software"), to deal in the
  Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall
  be included in all copies or substantial portions of the
  Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import sys
import json
import wfuzz
from docopt import docopt

# mine
from dictionary import Dictionary
from enums import Sector, Mode
from database import SimpleDb
from options import Options

class Paf:

    def __init__(self, options):

        # validate and store user options
        self.options = Options(options)

        # database
        self.db = SimpleDb()

        # dictionary
        self.dict = Dictionary(self.options.sector)

    def __del__(self):
        self.db.close()

    def addTransaction(self, params):
        self.db.query('INSERT INTO results VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', params)
        self.db.commit()

    def getTransactions(self, depth = None):

        sql = 'SELECT * FROM results'
        params = ()

        if depth != None:
            sql += ' WHERE depth=?'
            params = (depth,)

        result = self.db.query(sql, params)

        return result.fetchall()

    def fuzz(self, depth, baseurl, fstring, payloads, hc=[404]):

        for prefix in self.options.prefix:

            for suffix in self.options.suffix:

                for extension in self.options.extension:

                    if depth == (self.options.depth - 1):
                        ext = extension
                    else:
                        ext = ''

                    url = '%s/%s%s%s%s' % (
                        baseurl,
                        prefix,
                        fstring,
                        suffix,
                        ext,
                    )

                    # begin the actual fuzzing...

                    for r in wfuzz.get_payloads(payloads).fuzz(
                        dryrun=self.options.dryrun,
                        method=self.options.method,
                        hc=hc,
                        url=url):

                        # show some progress
                        print("  [+] %s %s" % (r.history.method, r.url), file=sys.stderr)

                        # log the found endpoint to database
                        self.addTransaction((
                            r.history.reqtime,
                            depth,
                            r.history.method,
                            json.dumps(r.history.headers.request),
                            r.history.pstrip,
                            r.url,
                            r.history.urlp.fext,
                            r.history.urlp.ffname,
                            r.history.urlp.fname,
                            r.history.urlp.fragment,
                            r.history.urlp.hasquery,
                            r.history.urlp.hostname,
                            r.history.urlp.netloc,
                            r.history.urlp.params,
                            r.history.urlp.password,
                            r.history.urlp.path,
                            r.history.urlp.port,
                            r.history.urlp.query,
                            r.history.urlp.scheme,
                            r.history.urlp.username,
                            r.code,
                            json.dumps(r.history.headers.response),
                            r.history.headers.response.get('Content-Type'),
                            r.chars,
                            r.history.content
                        ))

    def fuzzSegment(self, depth, baseurl):

        # lambdas to change case
        upper = lambda x: x.upper()
        lower = lambda x: x.lower()
        ucfirst = lambda x: x[:1].upper() + x[1:].lower()

        # BEGIN

        if Mode.noun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    )))
                ]
            )

        if Mode.verb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(lower, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.Noun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.Verb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.NOUN in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.VERB in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ',
                payloads=[
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.nounnoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.nounverb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(lower, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.verbnoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.nounXnoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.nounXverb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.verbXnoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.nounNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.nounVerb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.verbNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(lower, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.nounXNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.nounXVerb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.verbXNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.nounNounXVerb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZFUZ2Z%sFUZ3Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.verbXNounNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2ZFUZ3Z' % separator,
                    payloads=[
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NounNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NounVerb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.VerbNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NounXNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NounXVerb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VerbXNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NounNounXVerb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZFUZ2Z%sFUZ3Z' % separator,
                    payloads=[
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VerbXNounNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2ZFUZ3Z' % separator,
                    payloads=[
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NOUNNOUN in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NOUNVERB in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.VERBNOUN in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2Z',
                payloads=[
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NOUNNOUNVERB in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.VERBNOUNNOUN in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NOUNXNOUN in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NOUNXVERB in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z' % separator,
                    payloads=[
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VERBXNOUN in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZ%sFUZ2Z' % separator,
                payloads=[
                    list(map(upper, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(upper, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NOUNNOUNXVERB in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZFUZ2Z%sFUZ3Z' % separator,
                    payloads=[
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VERBXNOUNNOUN in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2ZFUZ3Z' % separator,
                    payloads=[
                        list(map(upper, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.nounNounVerb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    list(map(lower, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.verbNounNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    [line for line in self.dict.verbs],
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.NounNounVerb in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                ]
            )

        if Mode.VerbNounNoun in self.options.mode:
            self.fuzz(
                depth=depth,
                baseurl=baseurl,
                fstring='FUZZFUZ2ZFUZ3Z',
                payloads=[
                    list(map(ucfirst, self.dict.getVerbs(
                        original=not self.options.suppress,
                        past=self.options.past,
                        present=self.options.present,
                        verbable=self.options.verbable,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                    list(map(ucfirst, self.dict.getNouns(
                        original=not self.options.suppress,
                        plural=self.options.plural,
                    ))),
                ]
            )

        if Mode.nounXnounXverb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.verbXnounXnoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(lower, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(lower, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NounXNounXVerb in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VerbXNounXNoun in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(ucfirst, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(ucfirst, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

        if Mode.NOUNXNOUNXVERB in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                    ]
                )

        if Mode.VERBXNOUNXNOUN in self.options.mode:
            for separator in self.options.separator:
                self.fuzz(
                    depth=depth,
                    baseurl=baseurl,
                    fstring='FUZZ%sFUZ2Z%sFUZ3Z' % (separator,separator),
                    payloads=[
                        list(map(upper, self.dict.getVerbs(
                            original=not self.options.suppress,
                            past=self.options.past,
                            present=self.options.present,
                            verbable=self.options.verbable,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                        list(map(upper, self.dict.getNouns(
                            original=not self.options.suppress,
                            plural=self.options.plural,
                        ))),
                    ]
                )

    def runPathOperation(self):

        # fuzz the first segment
        self.fuzzSegment(depth=0, baseurl=self.options.baseurl)

        # process additional segments to depth limit
        for depth in range(1, self.options.depth):
            for transaction in self.getTransactions(depth - 1):
                self.fuzzSegment(depth=depth, baseurl=transaction[6])

        # debug
        if not self.options.dryrun:
            for transaction in self.getTransactions():
                print('[%05s] %s %s' % (
                    transaction[0],       # id
                    transaction[3],       # method
                    transaction[6],       # url
                ), file=sys.stdout)

    def runMethodOperation(self):
        print('Not implemented')

    def runParamOperation(self):
        print('Not implemented')

    def runInputOperation(self):
        print('Not implemented')

    def run(self):

        try:
            operations = {
                'path': self.runPathOperation,
                'method': self.runMethodOperation,
                'param': self.runParamOperation,
                'input': self.runInputOperation,
            }

            operation = self.options.operation

            operations.get(operation, lambda: print(
                'Operation "%s" unknown' % operation
            ))()

        except Exception as e:
            print('  [ERR] %s' % e)

arguments = docopt(__doc__)
paf = Paf(arguments)
paf.run()
