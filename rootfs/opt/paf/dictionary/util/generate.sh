#!/usr/bin/env bash

# usage: verb_conjugator [-h] -l LANG -i INPUT [-o OUTPUT]
# verb_conjugator: error: the following arguments are required: -l/--lang, -i/--input

# http://api.verbix.com/conjugator/html

python conjugate.py -l eng -i verbs.txt
