
FROM alpine:3.10

# This hack is widely applied to avoid python printing issues in docker containers.
# See: https://github.com/Docker-Hub-frolvlad/docker-alpine-python3/pull/13
ENV PYTHONUNBUFFERED=1

RUN apk update && apk upgrade --latest && \
    apk add --no-cache python3  && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade \
        pip \
        setuptools \
        wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    apk add --no-cache \
        bash \
        bash-completion \
        curl \
        curl-dev \
        gcc \
        iputils \
        jq \
        libressl-dev \
        musl-dev \
        ncurses \
        nmap \
        py-pysqlite \
        python3-dev \
        sqlite \
        sqlite-dev \
        tcpdump \
        tinyproxy \
        tmux \
        vim \
        wget && \
    pip3 install --no-cache --upgrade \
        docopt \
        hashid \
        inflect \
        shodan \
        wfuzz && \
    apk del \
        curl-dev \
        gcc \
        libressl-dev \
        musl-dev \
        python3-dev \
        sqlite-dev && \
    rm -f /tmp/* /etc/apk/cache/* && \
    adduser -D \
        -h /home/paf \
        -u 1000 \
        -s /bin/bash \
        paf \
        paf && \
    touch /tmp/paf.db && \
    chown paf:paf /tmp/paf.db && \
    chmod 600 /tmp/paf.db

COPY rootfs/home/paf /home/paf
RUN chown -R paf:paf /home/paf

# these wordlists will be mounted over for development
# COPY fsroot/opt/paf /opt/paf/nouns.txt
# COPY fsroot/opt/paf/dictionary/nouns.txt /opt/paf/nouns.txt
# COPY fsroot/opt/paf/dictionary/verbs.txt /opt/paf/verbs.txt

# copy the source code
# COPY rootfs/opt/paf/paf.py /opt/paf/paf.py

# install script to run paf
COPY rootfs/usr/local/bin/paf /usr/local/bin/paf
RUN chmod +x /usr/local/bin/paf

# for development
VOLUME ["/opt/paf"]

USER paf
WORKDIR /home/paf
ENTRYPOINT [ "/usr/local/bin/paf" ]
