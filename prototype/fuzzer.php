#!/usr/bin/env php
<?php
/**
    API Fuzzer

    Requires:
        - wfuzz (sudo apt install wfuzz)

    See https://wfuzz.readthedocs.io/en/latest/
*/

function usage(): void
{
    global $argv;

    printf(
        "Usage: %s [options] <URL> <WORDLIST>\n",
        $argv[0]
    );
}

function debug(string $message): void
{
    fprintf(
        STDERR,
        "[%s] %s\n",
        date('d/m/Y H:i:s'),
        $message
    );
}

function error(string $message): void
{
    debug(sprintf('ERROR: %s', $message));
}

function fatal(string $message): void
{
    debug(sprintf('FATAL: %s', $message));
    exit(1);
}

/*
Usage:  wfuzz [options] -z payload,params <url>

        FUZZ, ..., FUZnZ  wherever you put these keywords wfuzz will replace them with the values of the specified payload.
        FUZZ{baseline_value} FUZZ will be replaced by baseline_value. It will be the first request performed and could be used as a base for filtering.


Options:
        -h                          : This help
        --help                      : Advanced help
        --version                   : Wfuzz version details
        -e <type>                   : List of available encoders/payloads/iterators/printers/scripts

        -c                          : Output with colors
        -v                          : Verbose information.
        --interact                  : (beta) If selected,all key presses are captured. This allows you to interact with the program.

        -p addr                     : Use Proxy in format ip:port:type. Repeat option for using various proxies.
                                      Where type could be SOCKS4,SOCKS5 or HTTP if omitted.

        -t N                        : Specify the number of concurrent connections (10 default)
        -s N                        : Specify time delay between requests (0 default)
        -R depth                    : Recursive path discovery being depth the maximum recursion level.
        -L, --follow                : Follow HTTP redirections

        -u url                      : Specify a URL for the request.
        -z payload                  : Specify a payload for each FUZZ keyword used in the form of type,parameters,encoder.
                                      A list of encoders can be used, ie. md5-sha1. Encoders can be chained, ie. md5@sha1.
                                      Encoders category can be used. ie. url
                                      Use help as a payload to show payload plugin's details (you can filter using --slice)
        -w wordlist                 : Specify a wordlist file (alias for -z file,wordlist).
        -V alltype                  : All parameters bruteforcing (allvars and allpost). No need for FUZZ keyword.
        -X method                   : Specify an HTTP method for the request, ie. HEAD or FUZZ

        -b cookie                   : Specify a cookie for the requests
        -d postdata                 : Use post data (ex: "id=FUZZ&catalogue=1")
        -H header                   : Use header (ex:"Cookie:id=1312321&user=FUZZ")
        --basic/ntlm/digest auth    : in format "user:pass" or "FUZZ:FUZZ" or "domain\FUZ2Z:FUZZ"

        --hc/hl/hw/hh N[,N]+        : Hide responses with the specified code/lines/words/chars (Use BBB for taking values from baseline)
        --sc/sl/sw/sh N[,N]+        : Show responses with the specified code/lines/words/chars (Use BBB for taking values from baseline)
        --ss/hs regex               : Show/Hide responses with the specified regex within the content
*/

function fuzz(
    array &$results,
    ?array $payloads = [],
    ?string $baseurl = 'http://localhost/v1',
    ?string $method = 'HEAD',
    ?string $hidecodes = '404',
    ?array $headers = [],
    ?array $cookies = [],
    ?string $postdata = null,
    ?string $auth = null
) : void {

    $cmd = 'wfuzz -c -R 1 ';

    $cmd .= sprintf(
        '-X %s ',
        $method ?: 'HEAD'
    );

    if ($auth) {
        $cmd .= sprintf('--basic %s ', $auth);
    }

    if ($hidecodes) {
        $cmd .= sprintf('--hc %s ', $hidecodes);
    }

    if ($headers) {
        $buf= '';
        foreach ($headers as $header) {
            $buf .= sprintf('-H %s ', $header);
        }
        $cmd .= $buf;
    }

    if ($cookies) {
        $buf= '';
        foreach ($cookies as $cookie) {
            $buf .= sprintf('-b %s ', $cookie);
        }
        $cmd .= $buf;
    }

    if ($postdata) {
        $cmd = sprintf(
            '%s -d %s ',
            $cmd,
            $postdata
        );
    }

    $outfile = sprintf(
        '/tmp/fuzzer-%s.json',
        uniqid()
    );

    $cmd .= sprintf(
        '-f %s,json ',
        $outfile
    );

    if ($payloads) {
        $buf= '';
        foreach ($payloads as $payload) {
            $buf .= sprintf('-z %s ', $payload);
        }
        $cmd .= $buf;
    }

    $cmd .= $baseurl ?: 'http://localhost/v1';

    $cmd .= ' 1>wfuzz-output.log 2>&1';

    debug($cmd);

    exec($cmd);

    $response = json_decode(
        file_get_contents($outfile),
        true
    );

    $results = array_merge($results, $response);

}

function main()
{
    global $argc, $argv;

    if ($argc < 3)
    {
        usage();
        exit(2);
    }

    $url = rtrim($argv[1], '/');

    if (!filter_var($url, FILTER_VALIDATE_URL))
    {
        fatal(sprintf(
            'Invalid URL: %s', $url
        ));
    }

    $wordlist = $argv[2];

    if (!file_exists($wordlist)) {
        fatal(sprintf(
            'Invalid wordlist: %s', $wordlist
        ));
    }

    debug("Using URL: $url");
    debug("Using wordlist: $wordlist");

    $results = [];

    // ----------------------------------
    // 1) Scan first URI segment using HEAD
    // ----------------------------------

    fuzz(
        $results,
        [
            sprintf('file,%s', $wordlist),
        ],
        sprintf('%s/FUZZ', $url),
        'HEAD'
    );

    // ----------------------------------
    // 2) Scan second URI segment
    // ----------------------------------

    foreach ($results as $result) {
        fuzz(
            $results,
            [
                sprintf('file,%s', $wordlist),
            ],
            sprintf('%s/FUZZ', $result['url']),
            'HEAD',
            '404,401,405,500'
        );
    }

    // fetch content
    // fetch($results);

    // save results to file
    // outputJSON($results);

    // Print the results
    outputList($results);
}

function fetch(array &$results) : void
{
    foreach($results as &$result) {

        switch ($result['method']) {
            case 'HEAD':
                continue 2;
            default:
                break;
        }

        $cmd = sprintf(
            'curl -s -X %s %s 2>/dev/null',
            $result['method'],
            $result['url']
        );

        debug($cmd);

        $output = '';
        exec($cmd, $output);
        $response = join(PHP_EOL, $output);
        $data = json_decode($response, true);
        $result['response'] = $data;
    }
}

function outputJSON(array &$results) : void
{
    file_put_contents(
        'fuzzer-output.json',
        json_encode($results, JSON_PRETTY_PRINT)
    );
}

function outputList(array &$results) : void
{
    foreach($results as $result) {
        $extractMessage = function(array $result) : string {
            $message = '<unknown>';
            if ($result['code'] == 422) {
                // $message = $result['response']['detail'];
                $message = $result['response']['apiCode'];
            } elseif (empty($result['response'])) {
                $message = '<none>';
            } elseif (is_scalar($result['response'])) {
                $message = (string)$result['response'];
            } elseif (!empty($result['response']['message'])) {
                $message = $result['response']['message'];
            } elseif (!empty($result['response']['detail'])) {
                $message = $result['response']['detail'];
            } else {
                $message = join(' ', $result['response']);
            }
            return $message;
        };

        fprintf(
            STDOUT,
            "[%s] %s %s -> %s\n",
            $result['code'],
            $result['method'],
            $result['url'],
            $extractMessage($result)
        );
    }
}


main();
